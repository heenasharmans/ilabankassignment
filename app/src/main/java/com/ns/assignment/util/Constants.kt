package com.ns.assignment.util

object Constants {
    const val CAROUSEL_DATA = "imagedata.json"
    const val LIST_DATA = "testdata.json"
}